﻿using UnityEngine;

namespace Slidermax {
	/**
	 *  Simple transition qui fait déplacer un objet selon un mouvement continu durant un certain temps.
	 */
	public class TranslateTransition : Transition3D {

		private readonly Vector3 destination;
		private readonly float duration;

		public TranslateTransition(Transform m, Vector3 destination, float duration) : base(m) {
			this.destination = destination;
			this.duration = duration;
		}
		
		public override void StartTransition() {

			float speed = Vector3.Distance(destination, target.transform.position) / duration;

			Register((d) => {
				target.transform.position = Vector3.MoveTowards(target.transform.position, destination, speed * d);
			}, duration);

			StartTransitionInternal(false);
		}
	}
}
