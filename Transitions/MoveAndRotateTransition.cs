﻿using UnityEngine;

namespace Slidermax {
	/**
	 *  Transition pour faire bouger et tourner un objet.
	 *  Durant la première moitié du trajet, il va tourner dans ce sens, et durant la deruxième moitié il inverse le mouvement.
	 */
	public class MoveAndRotateTransition : Transition3D {

		private readonly Vector3 movement;
		private readonly Vector3 rotation;
		private readonly float totalDuration;

		public MoveAndRotateTransition(Transform m, float totalDuration, Vector3 movement, Vector3 rotation) : base(m) {
			this.totalDuration = totalDuration;
			this.movement = movement;
			this.rotation = rotation;
			Start();
		}

		public MoveAndRotateTransition(Transform m, float totalDuration, Vector3 movement)
			: this(m, totalDuration, movement, Vector3.zero) { }
		
		private void Start() {
			Register((d) => { // 1ère moitié
				target.Rotate(rotation * d);
				target.Translate(movement * d);
			}, totalDuration/2f);

			Register((d) => { // 2nde moitié à sens inversé.
				target.Rotate(rotation * -d);
				target.Translate(movement * -d);
			}, totalDuration / 2f);
		}

		public override void StartTransition() {
			StartTransitionInternal(true);
		}
	}
}
