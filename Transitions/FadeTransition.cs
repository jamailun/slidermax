﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Slidermax {
	/**
	 *  Transition qui peut faire disparaitre ou apparaitre un objet et ses enfants.
	 */
	public class FadeTransition {

		private readonly IList<Renderer> renderers;
		private Sequencer sequencerIn, sequencerOut;

		// @param alsoFadeChildren : si oui ou non on fait la même chose aux enfants.
		public FadeTransition(Renderer renderer, bool alsoFadeChildren) {
			renderers = new List<Renderer>() { renderer };
			if(alsoFadeChildren) {
				foreach (Renderer r in renderer.gameObject.GetComponentsInChildren<Renderer>()) {
					if(r.gameObject != renderer.gameObject)
						renderers.Add(r);
				}
			}
		}

		public void StartToFadeAway(float duration) {
			if (sequencerOut != null && sequencerOut.IsRunning())
				return;
			sequencerOut = new Sequencer();
			IDictionary<Renderer, float> speeds = BeforeFading(duration);
			sequencerOut.AddAction((d) => {
				foreach (Renderer renderer in renderers) {
					float currentA = GetColor(renderer).a;
					float save = GetColor(renderer).a;
					currentA = Mathf.Max(0f, currentA - (d * speeds[renderer]));
					UpdateAlpha(renderer, currentA);
					Debug.Log($"({save}) -> ({ GetColor(renderer).a}) car currentA={currentA}.");
				}
			}, duration);
			sequencerOut.StartConcurrent();
		}

		public void StartToFadeIn(float duration) {
			if (sequencerIn != null && sequencerIn.IsRunning())
				return;
			sequencerIn = new Sequencer();
			IDictionary<Renderer, float> speeds = BeforeFading(duration);
			sequencerIn.AddAction((d) => {
				foreach (Renderer renderer in renderers) {
					float currentA = GetColor(renderer).a;
					currentA = Mathf.Min(1f, currentA + (d * speeds[renderer]));
					UpdateAlpha(renderer, currentA);
				}
			}, duration);
			sequencerIn.StartConcurrent();
		}

		private IDictionary<Renderer, float> BeforeFading(float duration) {
			IDictionary<Renderer, float> speeds = new Dictionary<Renderer, float>();
			foreach (Renderer renderer in renderers) {
				//cas spécial : TMP
				if (renderer.gameObject.GetComponent<TextMeshPro>() != null) {
					speeds.Add(renderer, (renderer.gameObject.GetComponent<TextMeshPro>().color.a) / duration);
					continue;
				}
				speeds.Add(renderer, (renderer.material.color.a) / duration);
				// change tous les matérials en rendering mode 'Fade' (il peut alors être invisible)
				StandardShaderUtils.ChangeRenderMode(renderer.material, StandardShaderUtils.BlendMode.Fade);
			}
			return speeds;
		}

		private void UpdateAlpha(Renderer renderer, float alpha) {
			TextMeshPro tmp = renderer.gameObject.GetComponent<TextMeshPro>();
			if (tmp != null)
				tmp.color = MutateColor(tmp.color, alpha);
			else
				renderer.material.color = MutateColor(renderer.material.color, alpha);
		}

		private Color MutateColor(Color source, float newAlpha) {
			return new Color(source.r, source.g, source.b, newAlpha);
		}

		private Color GetColor(Renderer renderer) {
			if (renderer.gameObject.GetComponent<TextMeshPro>() != null)
				return renderer.gameObject.GetComponent<TextMeshPro>().color;
			return renderer.material.color;
		}

	}
}
