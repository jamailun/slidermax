﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using UnityEngine;

namespace Slidermax {
	public class Sequencer {

		private readonly MonoBehaviour root = SequencerHook.INSTANCE;
		private readonly IDictionary<Action<float>, float> steps = new Dictionary<Action<float>, float>();
		private readonly IDictionary<int, IEnumerator<YieldInstruction>> runnings = new ConcurrentDictionary<int, IEnumerator<YieldInstruction>>();

		public Sequencer() {
			if (root == null)
				throw new Exception("Create an instance of SequencerHook to use Sequencers.");
		}

		public void AddAction(Action<float> action, float duration = .01f) {
			steps.Add( action , duration );
		}
		
		public void StartConcurrent() {
			float currentWait = 0;
			int id = 1;
			foreach(KeyValuePair<Action<float>, float> pair in steps) {
				if(pair.Key == null) {
					root.StartCoroutine( StartWait(currentWait, pair.Value) );
					currentWait += pair.Value;
					continue;
				}
				IEnumerator<YieldInstruction> coro = StartAction(id, currentWait, pair.Key, pair.Value);
				runnings.Add(id++, coro);
				root.StartCoroutine( coro );
				currentWait += pair.Value;
			}
		}

		private IEnumerator<YieldInstruction> StartAction(int id, float waitDuration, Action<float> action, float duration) {
			yield return new WaitForSeconds(waitDuration);
			float started = Time.time;
			float lastCall = started;
			while (Time.time - started < duration) {
				yield return new WaitForFixedUpdate();
				action(Time.time - lastCall);
				lastCall = Time.time;
			}
			runnings.Remove(id);
		}

		private IEnumerator<YieldInstruction> StartWait(float waitDuration, float duration) {
			yield return new WaitForSeconds(waitDuration + duration);
		}

		public void Stop() {
			foreach(IEnumerator<YieldInstruction> coro in runnings.Values) {
				root.StopCoroutine(coro);
			}
			runnings.Clear();
		}

		public bool IsRunning() {
			return runnings.Count > 0;
		}
		
	}
}
