﻿using UnityEngine;

public class SequencerHook : MonoBehaviour {
	public static SequencerHook INSTANCE;
	private void Awake() {
		if (INSTANCE != null) {
			Debug.LogError("Error : two SequencerHook exist. Removing one (" + name + ").");
			Destroy(gameObject);
			return;
		}
		INSTANCE = this;
	}
}
