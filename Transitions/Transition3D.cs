﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Slidermax {
	public abstract class Transition3D {

		protected readonly Transform target;

		private Sequencer sequencer;
		private bool finalized;
		private Vector3 startedPos, startedScale;
		private Quaternion startedRotation;
		private bool running = false;

		public Transition3D(Transform target) {
			this.target = target;
			sequencer = new Sequencer();
			finalized = false;
		}

		protected void Register(Action<float> action, float duration) {
			if (finalized)
				throw new Exception("Cannot register action after a start.");
			sequencer.AddAction(action, duration);
		}

		protected void StartTransitionInternal(bool revertable) {
			if (running)
				return;
			running = true;
			if(revertable) {
				// Si il faut revert à la fin, on sauvergarde la position actuelle.
				startedPos = target.transform.position;
				startedRotation = target.transform.rotation;
				startedScale = target.transform.localScale;
				// Si premier lancement, il faut rajouter l'étape de restauration du revert si il est présent. 
				if (!finalized) {
					Register((d) => {
						target.position = startedPos;
						target.rotation = startedRotation;
						target.localScale = startedScale;
					}, 0.01f);
				}
			}
			if (!finalized) { // Si premier lancement, on rajoute un reset de running à la fin.
				Register((d) => {
					running = false;
				}, 0.01f);
			}

			// Lancement effectif

			finalized = true;
			sequencer.StartConcurrent();
		}

		public bool IsRunning() {
			return running;
		}

		public abstract void StartTransition();

	}
}
