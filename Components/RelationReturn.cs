﻿using System;
using FIVE.Unity;

public class RelationReturn : UFRelation
{
    [UFObjectPattern("TypeCube", "Cube")]
    public TypeColored ColoredObject1;

    [UFObjectPattern("User", "Sphere")]
    public TypeColored ColoredObject2;

    [UFObjectPattern("User", "User")]
    public TypeUser UserObject;

    [UFObjectPattern("Diaporama", "Diaporama")]
    public TypeDiaporama DiaporamaObject;

    public override bool IsRunnable()
    {
        return ColoredObject1.hasBeenCollided && ColoredObject2.hasBeenCollided;
    }

    public override void Run(Action resultCallback)
    {
        ColoredObject1.ReturnColor(ColoredObject2.gameObject);
        DiaporamaObject.ReturnDiaporama();
        resultCallback();
    }
}

