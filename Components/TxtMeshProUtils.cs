﻿using UnityEngine;
using TMPro;
using ODP.API;
using ODP.Utils;
using System;

namespace Slidermax {
	public class TxtMeshProUtils {
		private TxtMeshProUtils() {}

		/**
		 *  Appliquer à un TMPro les effets simples d'un style.
		 */
		public static void ApplyStyleOnTMPro(TextMeshPro txtMesh, IStyle style) {

			if (style.HasFontColor()) { // Couleur
				if (ColorUtility.TryParseHtmlString(style.GetFontColor(), out Color txtColor)) //usage d'un out par la documentation
					txtMesh.color = txtColor;
				else
					Debug.LogError($"Echec de conversion d'une valeur de color : {style.GetFontColor()}."); // simple log en cas d'erreur pour le moment.
			} else {
				txtMesh.color = Color.black;
			}

			if (style.HasAreaVerticalAlign()) { // Alignement vertical
				switch (style.GetAreaVerticalAlign()) {
					case "middle":
						txtMesh.verticalAlignment = VerticalAlignmentOptions.Middle;
						break;
					case "bottom":  // Non testé
						txtMesh.verticalAlignment = VerticalAlignmentOptions.Bottom;
						break;
					case "top":     // non testé + non nécessaire ?
						txtMesh.verticalAlignment = VerticalAlignmentOptions.Top;
						break;
				}
			}

			if (style.HasTextAlign()) { // Alignement horizontal
				switch (style.GetTextAlign()) {
					case "left":    // Non testé + non nécessaire ?
						txtMesh.horizontalAlignment = HorizontalAlignmentOptions.Left;
						break;
					case "right":   // Non testé
						txtMesh.horizontalAlignment = HorizontalAlignmentOptions.Right;
						break;
					case "center":
						txtMesh.horizontalAlignment = HorizontalAlignmentOptions.Center;
						break;
					case "justified":
						txtMesh.horizontalAlignment = HorizontalAlignmentOptions.Justified;
						break;
				}
			}

			if (style.HasPaddingData())
				SpecialsAlignements(txtMesh, style.GetPadding());

			if (style.HasFontStyle()) {     // Effets simples.
				ODP.Utils.FontStyle fs = style.GetFontStyle();

				FontStyles tmproStyle = FontStyles.Normal;
				if(fs.HasPropertie(ODP.Utils.FontStyle.FontStyleType.Bold))
					tmproStyle |= FontStyles.Bold;
				if (fs.HasPropertie(ODP.Utils.FontStyle.FontStyleType.Italic))
					tmproStyle |= FontStyles.Italic;
				if (fs.HasPropertie(ODP.Utils.FontStyle.FontStyleType.Underlined))
					tmproStyle |= FontStyles.Underline;
				if (fs.HasPropertie(ODP.Utils.FontStyle.FontStyleType.Linestrike))
					tmproStyle |= FontStyles.Strikethrough;

				txtMesh.fontStyle = tmproStyle; // On utilise une addition de flags
			}
		}

		private static void SpecialsAlignements(TextMeshPro txtMesh, SquareSize squareSize) {
			// Padding qui centre verticalement ?
			if (squareSize.HasSize(SquareSize.SquareSizeType.Bottom) && squareSize.HasSize(SquareSize.SquareSizeType.Top)) {
				if(squareSize.GetSize(SquareSize.SquareSizeType.Bottom).Value == squareSize.GetSize(SquareSize.SquareSizeType.Top).Value) {
					txtMesh.verticalAlignment = VerticalAlignmentOptions.Middle;
				}
			}
			// Padding qui centre horizontalement ?
			if (squareSize.HasSize(SquareSize.SquareSizeType.Right) && squareSize.HasSize(SquareSize.SquareSizeType.Left)) {
				if (squareSize.GetSize(SquareSize.SquareSizeType.Right).Value == squareSize.GetSize(SquareSize.SquareSizeType.Left).Value) {
					txtMesh.horizontalAlignment = HorizontalAlignmentOptions.Center;
				}
			}
		}
	}
}
