﻿using ODP.API;
using UnityEngine;
using TMPro;

namespace Slidermax {
	internal class SlideTextBox {

		const float MULTIPLICATIVE_FONT_SIZE = 2f; // Approximation de la différence de taille de police entre Unity TMPro et ODP.

		private readonly GameObject txtObject;

		// Comme un IPLine n'est pas stylable, on passe le style du parent (TextBox (et si il n'existe pas, on prend la frame)).
		public SlideTextBox(Transform origin, IPLine line, IStyle style, string name, Vector2 parentsSizes, bool firstInParagraph, float dy) {
			// On instancie un nouvel objet
			txtObject = new GameObject(name);
			
			// On met comme parent notre origine passée en paramètre
			txtObject.transform.SetParent(origin);

			// On ajoute un textMesh component.
			TextMeshPro txtMesh = txtObject.AddComponent<TextMeshPro>();
			// On met le bon texte

			float maxFontSize = 1;
			txtMesh.text = "";

			// Si jamais la ligne a un style de 'line item' on applique le décalage qui en découle
			float dx = 0;
			if(line.HasLineItem()) {
				if(firstInParagraph)	// important car le point ne s'affiche que lorsque c'est la première ligne d'un paragraphe
					txtMesh.text = line.LineItem.GetBulletChar() + " ";
				dx = line.LineItem.GetMinLabelWidth().Value / 9f;
			}

			// Ici il faudrait dans l'idéal découper la ligne en plusieurs morceaux de texte, pour changer le style au caractère près.
			foreach(ISpan span in line.Enumerate()) {
				if (span.HasStyle())
					style = span.Style.DerivateWithParent(style);

				txtMesh.text += span.Text.Replace("<numéro>", ""+SliderRenderer.currentDiapo);
				if (style.HasFontSize()) {
					txtMesh.fontSize = style.GetFontSize() * MULTIPLICATIVE_FONT_SIZE;
					if (style.GetFontSize() > maxFontSize)
						maxFontSize = style.GetFontSize();
				}
			}

			maxFontSize *= MULTIPLICATIVE_FONT_SIZE; // la taille des fonts dans ODP et dans TMPro n'est pas la même. J'ai créé cette constante pour passer de l'un à l'autre.
			float boxHeight = maxFontSize / 10f;

			// Application des styles (alignement, couleur, gras, ...).
			TxtMeshProUtils.ApplyStyleOnTMPro(txtMesh, style);

			// On déplace le textMesh au bon endroit
			txtObject.transform.localPosition = new Vector3(
				SliderRenderer.UNITY_PLANE_DIM_X / 2f + dx,
				- dy,
				-0.01f  // éviter de se mélanger avec la couche précédente.
			);

			// On lui donne la bonne taille. (sur X on copie le parent, sur Y on prend simplement l'espace nécessaire pour la police).
			txtObject.GetComponent<RectTransform>().sizeDelta = new Vector2(
				SliderRenderer.UNITY_PLANE_DIM_X * parentsSizes.x,
				boxHeight
			);
			
		}

		public void Destroy() {
			Object.Destroy(txtObject);
		}

	}
}
