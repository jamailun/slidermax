﻿using UnityEngine;

public class TransitionsRenderer : MonoBehaviour {
    [SerializeField] public Transition transition;
    private int nbLoop = 0;
    private bool tmp = true;

    private Animator animator;
    private void Start() {
        animator = Instantiate(transition).animator;
    }

	public void AnimateRight() {
		if (nbLoop == 0 && tmp == true) {
			animator.SetTrigger("Start");
			tmp = false;
		}
		animator.SetTrigger("StartLoop");
		nbLoop++;
	}

	public void AnimateLeft() {
		animator.SetTrigger("StartLoop");
	}
	
}
