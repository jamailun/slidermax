﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using ODP.API;
using ODP.Generics;
using Texture = ODP.Generics.Texture;
using static ODP.Utils.Size;
using System.Linq;

namespace Slidermax {
	public class SlideFrame {

		private const float HOW_MANY_CENTIMETERS_IN_ONE_PT = 0.0352778f; // pour convertir les tailles de polices en centimètres

		private readonly GameObject planeObject;
		private readonly IList<SlideTextBox> textes = new List<SlideTextBox>();
		private readonly IList<SVGRenderer> svgr = new List<SVGRenderer>();

		internal SlideFrame(Transform origin, IFrame frame, string objectName, Vector2 unityDimensions, float deltaY) {
			IStyle style = frame.Style;

			planeObject = GameObject.CreatePrimitive(PrimitiveType.Plane);  // Création du plan 5/5, et setup de base
			planeObject.name = objectName;
			planeObject.transform.SetParent(origin);

			float sizeX = SizeAdaptater.ConvertODPUnits(frame.Width, SliderRenderer.ODP_SLIDE_WIDTH);   // Transformation des échelles en cm en 5/5 sur Unity.
			float sizeZ = SizeAdaptater.ConvertODPUnits(frame.Height, SliderRenderer.ODP_SLIDE_HEIGHT);

			planeObject.transform.localScale = new Vector3(sizeX, 1f, sizeZ); // Application de la taille
			planeObject.transform.localRotation = Quaternion.identity; // reset la rotation

			// pos_# = ((rapport sur dimension dans odp #) * dimensions_# dans unity) + la moitié de la taille # dans unity. (#={x,y})
			float odpX = SizeAdaptater.ConvertODPUnits(frame.X, SliderRenderer.ODP_SLIDE_WIDTH) * SliderRenderer.UNITY_PLANE_DIM_X;
			float odpZ = SizeAdaptater.ConvertODPUnits(frame.Y, SliderRenderer.ODP_SLIDE_HEIGHT) * SliderRenderer.UNITY_PLANE_DIM_Z;

			float distX = sizeX * 5f;   // Moitié de la taille de la dimension x
			float distZ = sizeZ * 5f;   // idem sur z

			planeObject.transform.localPosition = new Vector3( // Position = (équivalent odp (coin nord-est)) + (moitié de la taille dans cette dimension (dans le contexte plan 5/5)).
				distX + odpX,
				deltaY,
				-(distZ + odpZ)
			);

			if (style != null && style.HasFillData()) {
				if(style.GetFill().FillColor == "none") {
					planeObject.GetComponent<MeshRenderer>().enabled = false;
				} else {
					ColorUtility.TryParseHtmlString(style.GetFill().FillColor, out Color fillColor);
					planeObject.GetComponent<MeshRenderer>().material.color = fillColor;
				}
			} else {
				planeObject.GetComponent<MeshRenderer>().enabled = false;
			}

			Transform childTransform = new GameObject("origin").GetComponent<Transform>();
			childTransform.SetParent(planeObject.transform);

			childTransform.localScale = (new Vector3(1f, 1f, 1f));
			childTransform.localPosition = (new Vector3(-5f, 0f, 5f));

			var parentsSizes = new Vector2( // taille globale des parents (utilisée dans la création des sous composants)
				sizeX * unityDimensions.x,
				sizeZ * unityDimensions.y
			);

			//Debug.LogWarning(">"+frame.GetContent().GetType());

			if (frame.GetContent().GetType() == typeof(TextBox)) {
				CreateTextBox(((TextBox)frame.GetContent()).GetParagraphs(), ((TextBox)frame.GetContent()).Style, parentsSizes, childTransform, Count((TextBox)frame.GetContent()));
				StandardShaderUtils.ChangeRenderMode(planeObject.GetComponent<MeshRenderer>().material, StandardShaderUtils.BlendMode.Fade);
			} else if (frame.GetContent().GetType() == typeof(ImageBox)) {
				ApplyTexture(
					((ImageBox)frame.GetContent()).GetImage(),
					planeObject.GetComponent<MeshRenderer>(),
					0.6f
				);
			} else if (frame.GetContent().GetType() == typeof(Texture)) {
				ApplyTexture(
					((Texture)frame.GetContent()).GetImage(),
					planeObject.GetComponent<MeshRenderer>(),
					1f
				);
			} else if (frame.GetContent().GetType() == typeof(CustomShape)) {
				planeObject.name = "shape_" + planeObject.name;
				SVGRenderer svg = planeObject.AddComponent<SVGRenderer>();
				CustomShape shape = (CustomShape)frame.GetContent();
				svg.ApplySVG(
					shape,
					style,
					new Vector2(
						SizeAdaptater.ConvertODPUnits(frame.Width, SliderRenderer.ODP_SLIDE_WIDTH) * SliderRenderer.UNITY_PLANE_DIM_X * 2,
						SizeAdaptater.ConvertODPUnits(frame.Height, SliderRenderer.ODP_SLIDE_HEIGHT) * SliderRenderer.UNITY_PLANE_DIM_Z * 2
					),
					childTransform,
					new Vector2(
						planeObject.transform.position.x / 2,
						planeObject.transform.position.z / 2
					)
				);
				// Car les shapes peuvent avoir du texte, le voici :
				IList<IList<IPLine>> pps = new List<IList<IPLine>> {
					new List<IPLine> {
						shape.GetText()
					}
				};
				CreateTextBox(pps, frame.Style, parentsSizes, childTransform, 1);
			} else {
				Debug.LogWarning($"Type of the frame '{objectName}' : {frame.GetContent().GetType()}.");
			}
		}

		private void CreateTextBox(IList<IList<IPLine>> paragraphs, IStyle tbStyle, Vector2 parentsSizes, Transform childTransform, int count) {
			textes.Clear(); // reset de la liste

			// Le positionnement des boites de texte dépend de l'orientation de celui-ci.
			// Cas où le texte est verticalement orienté en HAUT, on a un delta = 0.
			float lineHeight = tbStyle.GetFontSize() * HOW_MANY_CENTIMETERS_IN_ONE_PT;
			float deltaVerti = lineHeight / 2;
			// Dans le cas où le texte est orienté verticalement au CENTRE, le delta est (hauteur de la slide/2 - (nombre_lignes*haueur_ligne / 2)).
			float addi = SizeAdaptater.ConvertODPUnits(ParseString((SliderRenderer.CENTIMETERS_SLIDE_HEIGHT / 2f) - ((count * lineHeight) / 2f) + "cm"), SliderRenderer.ODP_SLIDE_HEIGHT) * 10;
			if (tbStyle.HasAreaVerticalAlign() && tbStyle.GetAreaVerticalAlign() == "middle") {
				// CENTRE
				deltaVerti += addi;
			} else if (tbStyle.HasAreaVerticalAlign() && tbStyle.GetAreaVerticalAlign() == "top") {
				deltaVerti += addi;
			//	Debug.LogWarning("TEXT > "+ paragraphs.First().First().Enumerate().First().Text);
			//	Debug.LogWarning("STYLE > " + tbStyle);
			}

			int lineSize = count;
			int para = 1;
			int realLine = 0;
			foreach (List<IPLine> paragraph in paragraphs) {
				int lineN = 0;
				foreach (IPLine line in paragraph) {

					SlideTextBox text = new SlideTextBox(childTransform, line, tbStyle, $"Text_{para}-{lineN}", parentsSizes, lineN == 0, deltaVerti);
					deltaVerti += lineHeight;

					textes.Add(text);
				//	Debug.Log("H" );
				//	Debug.Log("Line="+line+", style=" + tbStyle);

					lineN++;
					realLine++;
				}
				para++;
			}
		}

		private void ApplyTexture(MemoryStream image, MeshRenderer mesh, float metallic) {
			Texture2D texture = BytesToTexture2D(image.ToArray());
			mesh.material.mainTexture = texture;
			mesh.enabled = true;
			mesh.material.mainTextureScale = new Vector2(-1, -1);
			mesh.material.SetFloat("_Metallic", metallic);
			mesh.material.SetFloat("_Glossiness", 0.34f);
			StandardShaderUtils.ChangeRenderMode(mesh.material, StandardShaderUtils.BlendMode.Fade);
		}

		public Texture2D BytesToTexture2D(byte[] imageBytes) {
			Texture2D tex = new Texture2D(2, 2);
			tex.LoadImage(imageBytes);
			return tex;
		}

		public void Destroy() {
			foreach(SlideTextBox box in textes)
				box.Destroy();
			foreach(SVGRenderer sr in svgr)
				sr.ClearLines();
			Object.Destroy(planeObject);
		}

		private int Count(TextBox box) {
			int sum = 0;
			foreach (List<IPLine> paragraph in box.GetParagraphs())
				sum += paragraph.Count;
			return sum;
		}

	}
}
