﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Drawing;
using Unity.VectorGraphics;
using ODP.API;
using ODP.Generics;
using System.Text.RegularExpressions;
using Color = UnityEngine.Color;
using org.mariuszgromada.math.mxparser;
using System;
// Idée de B.Arnaldi : Faire une "photo en svg" à la sortie d'odp puis l'intégrer sur unity avec le package (ça n'a pas l'air faisable :( )

namespace Slidermax {
	public class SVGRenderer : MonoBehaviour {

		public static readonly Color DEFAULT_COLOR = new Color(114, 159, 207);

		private SvgLine[] lines = new SvgLine[0];
		private IDictionary<string, string> functionsEqu = new Dictionary<string, string>();

		public Material mat;

		public const string REGEX_VIEWBOX = "^<.*viewBox=(\\d+)\\s(\\d+)\\s(\\d+)\\s(\\d+).*$";
		//public const string REGEX_TYPE = "^<.*type=\"([^\"]*)\".*$";
		//public const string REGEX_PATH = "^<.*enhanced-path=\"([^\"]*)\".*$";

		public void ApplySVG(CustomShape shape, IStyle context, Vector2 sizes, Transform parent, Vector2 origine) {
			string path = shape.EnhancedPath;
			string shapeType = shape.ShapeType;

			Viewbox box = new Viewbox {
				x = 0,
				y = 0,
				width = 21600,
				height = 21600
			};

			foreach (Match match in Regex.Matches(shape.GetRawSvg(), REGEX_VIEWBOX, RegexOptions.IgnoreCase)) {
				box.x = int.Parse(match.Groups[1].Value);
				box.y = int.Parse(match.Groups[2].Value);
				box.width = int.Parse(match.Groups[3].Value);
				box.height = int.Parse(match.Groups[4].Value);
			}

			//Debug.Log($"VIEWBOX : ({box.x}, {box.y}, {box.width}, {box.height})");
			//Debug.Log($"sizes : ({sizes.x}, {sizes.y}), origine : ({origine.x}, {origine.y})");
			DrawSVGAsLines(path.Replace("?", ""), context, shape, sizes, box, origine);
		}

		private void DrawSVG(SvgObject svg, Transform parent) {
			//GameObject o = new GameObject(typeof(SvgObject)+"_shape"); // create a new game object
			//o.transform.SetParent(parent);
			svg.Draw();
		}

		private void AppendLine(SvgLine l) {
			SvgLine[] nl = new SvgLine[lines.Length + 1];
			for (int i = 0; i < lines.Length; i++)
				nl[i] = lines[i];
			nl[lines.Length] = l;
			lines = nl;
		}

		public void ClearLines() {
			lines = new SvgLine[0];
			functionsEqu.Clear();
		}

		private void DrawSVGAsLines(string enhancedPath, IStyle context, CustomShape cs, Vector2 sizes, Viewbox box, Vector2 origine) {
			if(enhancedPath.Contains("$")) {
				Debug.LogWarning("["+enhancedPath+"] have a '$' in it : path ignored."); // pas le temps de le faire cette année.
				return;
			}
			SvgShape shape = new SvgShape();
			string[] tokens = enhancedPath.Split(' ');
			var args = new Dictionary<string, float> {
				{ "logwidth", sizes.x },
				{ "logheight", sizes.y }
			};
			var csa = cs.Arguments();
			foreach (string entry in csa.Keys) {
				string eq = csa[entry];
				bool over = false;
				string DEBUG = entry + " => ";
				while(!over) {
					over = true;
					foreach (string f in csa.Keys) {
						if(f != entry) {
							if (eq.Contains(f)) {
								eq = eq.Replace(f, "("+csa[f]+")");
								over = false;
							}
						}
					}
				}
				DEBUG += eq + " => ";
				over = false;
				while (!over) {
					over = true;
					foreach (string f in args.Keys) {
						if (eq.Contains(f)) {
							eq = eq.Replace(f, (args[f] + "").Replace(",","."));
							over = false;
						}
					}
				}
				args[entry] = (float) new Expression(eq).calculate();
				//Debug.Log(DEBUG + eq + " =>>> " + args[entry]);
			}
			float a, b;
			bool doingL = false;
			for (int i = 0; i < tokens.Length; i++) {
				switch (tokens[i]) {
					case "M": // set le point initial
						a = GetValue(tokens[i + 1], context, args);
						b = GetValue(tokens[i + 2], context, args);
						doingL = false;
						shape.initialPoint = new Vector2(a, b);
						shape.currentPoint = new Vector2(a, b);
						shape.verticesList = new List<Vector2>();
						shape.lines = new List<SvgLine>();
						i += 2;
						break;
					case "L": // ajoute
						a = GetValue(tokens[i + 1], context, args);
						b = GetValue(tokens[i + 2], context, args);
						doingL = true;
						var newPoint = new Vector2(a, b);
						shape.lines.Add(new SvgLine { a = shape.currentPoint, b = newPoint });
						shape.currentPoint = newPoint;
						i += 2;
						break;
					case "Z": // retourne au début
						doingL = false;
						shape.lines.Add(new SvgLine { a = shape.currentPoint, b = shape.initialPoint });
						break;
					case "N": // fin
						break;
					default: // si plusieurs L à la suite, ce dernier est omis
						if (doingL) {
							a = GetValue(tokens[i], context, args);
							b = GetValue(tokens[i + 1], context, args);
							//shape.verticesList.Add(new Vector2(a, b));
							var newPointL = new Vector2(a, b);
							shape.lines.Add(new SvgLine { a = shape.currentPoint, b = newPointL });
							shape.currentPoint = newPointL;
							i++;
						} else {
							Debug.LogError("Unknown shape statement (" + tokens[i] + "). path = " + enhancedPath);
						}
						break;
				}
			}

			CalculateLinesForShape(shape, context, box, sizes, origine);
		}

		private float GetValue(string entry, IStyle context, IDictionary<string, float> arguments) {
			string value = entry;
			foreach (var pair in arguments.AsEnumerable()) {
				value = value.Replace(pair.Key, pair.Value + "");
			}
			Expression expression = new Expression(value.Replace(",", "."));
			try {
				float parsed = (float)expression.calculate();
				return parsed;
			} catch (FormatException) {
				Debug.LogError($"Impossible de parser l'expression SVG : [{entry}] <=> [{arguments[entry.Substring(1)]}].");
				return 0;
			}
		}

		private SvgObject CalculateLinesForShape(SvgShape shape, IStyle context, Viewbox box, Vector2 sizes, Vector2 origine) {
			foreach(SvgLine line in shape.lines) {
				var aBase = new Vector2((line.a.x + box.x) / box.width, (line.a.y + box.y) / box.height); // [0,1]
				var bBase = new Vector2((line.b.x + box.x) / box.width, (line.b.y + box.y) / box.height); // [0,1]
				var av = new Vector3((aBase.x * sizes.x) + origine.x, 0, (aBase.y * sizes.y) + origine.y);
				var bv = new Vector3((bBase.x * sizes.x) + origine.x, 0, (bBase.y * sizes.y) + origine.y);
				//Debug.Log("[a=" + aBase + ", b=" + bBase + "] <==> [a=" + av + ", b=" + bv + "]");

				SvgLine l2 = new SvgLine {
					a = av,
					b = bv,
					svgOwner = this,
					width = context.HasStrokeWidth() ? SizeAdaptater.ConvertODPUnits(context.GetStrokeWidth(), SliderRenderer.ODP_SLIDE_HEIGHT) : 0.1f
				};

				AppendLine(l2);
			}
			return null;
		}

		private struct Viewbox {
			public float x, y, width, height;
		}

		private struct SvgShape {
			public Vector2 initialPoint;
			public Vector2 currentPoint;
			public IList<Vector2> verticesList;
			public IList<SvgLine> lines;
		}

		private class SvgObject {
			public Color color = new Color(114, 159, 207);
			public virtual void Draw() {}
		}
		 
		private class SvgLine : SvgObject {
			public Vector3 a, b;
			public float width = 0.02f;
			public SVGRenderer svgOwner;
			public override void Draw() {
				svgOwner.AppendLine(this);
			}
		}

		/**
		 * La fin de ce fichier a été récupérer dans les documentations d'Unity, onglet de l'API Low Graphics (GL)
		 */

		// When added to an object, draws colored rays from the
		// transform position.
		public int lineCount = 100;
		public float radius = 3.0f;

		static Material lineMaterial;
		static void CreateLineMaterial() {
			if (!lineMaterial) {
				// Unity has a built-in shader that is useful for drawing
				// simple colored things.
				Shader shader = Shader.Find("Hidden/Internal-Colored");
				lineMaterial = new Material(shader) {
					hideFlags = HideFlags.HideAndDontSave
				};
				// Turn on alpha blending
				lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
				lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
				// Turn backface culling off
				lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
				// Turn off depth writes
				lineMaterial.SetInt("_ZWrite", 0);
			}
		}

		// Will be called after all regular rendering is done
		public void OnRenderObject() {
			CreateLineMaterial();

			// Apply the line material
			lineMaterial.SetPass(0);

			GL.PushMatrix();
			// Set transformation matrix for drawing to
			// match our transform
			GL.MultMatrix(transform.localToWorldMatrix);

			// Draw lines
			foreach(SvgLine line in lines) {
				GL.Begin(GL.LINES);
				for (float j = 0; j < line.width; j += 0.02f) {
					GL.Color(line.color);
					GL.Vertex3(line.a.x, line.a.y, line.a.z);
					GL.Vertex3(line.b.x, line.b.y, line.b.z + j);
				}
				GL.End();
			}
			
			GL.PopMatrix();
		}
	}
}