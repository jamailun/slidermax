﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ODP;
using ODP.Utils;
using ODP.API;
using System.Collections;
using ODP.Transitions;
using System.Xml.Linq;
using Slidermax;

public class Animation : MonoBehaviour {

	// Start is called before the first frame update
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}


	public void GetTransitions(ISlide slide, Sequencer sequencer) {
		//Identifie tous les ID de la slide
		if (slide.TimingRoot != null) {
			APIXmlUnique Objet;

			ITransitionsSequence Seq = slide.TimingRoot.Sequence;

			TransitionCondition tc = Seq.GetCurrentCondition();

			bool equalizer = true;
			String[] valF = new string[4];
			String id = "";
			float dura = 0f;
			float begin = 0f;

			while (Seq.HasMore() || equalizer) {

				//Gère le dernier tour de boucle
				if (!Seq.HasMore() && equalizer) {
					equalizer = !equalizer;
				}

				//On détermine le type de condition pour activer la transition
				//Cas None non traité ici (pas d'animation)

				//Animation déclenchée après attente
				if (tc.Type is TransitionConditionType.WaitMillis) {
					float wait = tc.MillisParam;
					Debug.Log("On attend : " + wait);

					sequencer.AddAction((d) => {
						WaitingTime(wait);
					});

				}

				//Animation déclenchée au clic
				if (tc.Type is TransitionConditionType.WaitClick) {
					Debug.Log("On attend un click : ");
					sequencer.AddAction((d) => {
						WaitingClick();
					});
				}


				//Récupération de tous les Nodes de la transition actuelle

				foreach (ITransitionNode Node in Seq.GetCurrentTransitionsBlock()) {

					dura = Node.Duration;
					begin = Node.BeginTime;
					id = Node.TargetElementID;
					Objet = slide.GetXmlIdInChildren(id);
					String[] val = null;

					//Réinitialisation en cas de NodeSet (nouvelle Animation)
					if (Node is NodeSet) {
						valF = new string[4];
					}

					//Ajout des valeurs du NodeAnimate dans valF
					if (Node is NodeAnimate) {
						val = ((NodeAnimate)Node).Values;
						addVal(val, valF);
					}

					Debug.Log(dura + " / " + begin + " / " + id);
					Debug.Log(slide.GetXmlIdInChildren(id));

					
				}

				//Traitement du cas de l'apparition (n'est pas une Transition3D)
				if(valF[0] == null) {
					
					sequencer.AddAction((d) => {
						MeshRenderer renderer = new MeshRenderer();
						FadeTransition ft = new FadeTransition(renderer, true);
						ft.StartToFadeIn(dura);
					});
				}

				//Traitement des autres cas de Transition3D
				if (valF[0] != null) {
					Debug.Log(valF);
					sequencer.AddAction((d) => {
						Transition3D t = identifieTransition(slide, valF, id, dura, begin);
						t.StartTransition();
					});
				}

				//On accepte la condition pour récupérer les autres
				Seq.ConditionAccepted();

			}


		}

		//A faire plus tard : retour en arriere...
		if (Input.GetKeyDown(KeyCode.LeftArrow)) {
			slide.TimingRoot.Sequence.ForcePrevious();
		}
	}




	//identifie la transition (3D) et la retourne pour l'ajouter dans le sequencer
	//il va falloir peut etre d'autres attributs en params
	public Transition3D identifieTransition(ISlide slide, String[] values, String id, float duration, float begin) {
		if(values == null) {
			//Cas ou c'est une apparition (Fade transition)
			//normalement déjà géré
        }

        if (values[0] == "x" && values[1] == "x" && values[2] == "y" && values[3] == "1+height/2") {
			//(Translate transition) la frame part du bas et va vers le haut (Balayage haut sous ODP)
			Vector3 vect = new Vector3(0, 0.005f, 0);

			MeshRenderer renderer = new MeshRenderer();

			
			return new TranslateTransition(renderer.transform, vect ,duration);
        }

		//return null pour compiler 
		return null;

    }


	//ajoute les valeurs de val1 dans val2
	public void addVal(String[] val1, String[] val2) {
		int indice = 0;

		while(val2[indice] !=null && indice < val2.Length) {
			indice++;
        }
		
		foreach(String v in val1) {
			if(indice < val2.Length) {
				val2[indice] = v;
				indice++;
			}
        }
	}


	public IEnumerator WaitingTime(float wait) {
		yield return new WaitForSeconds(wait);
	}

	public IEnumerator WaitingClick() {
		yield return new WaitUntil(new Func<bool>(()=> {
			if ((Input.GetKeyDown(KeyCode.RightArrow) || Input.GetMouseButtonDown(1))) {
				return true;
			}
			return false;
		}));
	}

		
}


// Premier tests
//foreach (NodesSequence Node in Block.GetNodesSequences()) {

	//TransitionType a;
	//switch (Node.Type) {
		//case TransitionType.Next:
			//Debug.Log("Next"); 
			//a = TransitionType.Next;
			//break;

		//case TransitionType.OnClick:
			//Debug.Log("OnClick");
		//	a = TransitionType.OnClick;
		//	break;

		//case TransitionType.SameTime:
		//	Debug.Log("MemeTemps");
		//	a = TransitionType.SameTime;
		//	break;

	//	case TransitionType.AfterFinished:
	//		Debug.Log("ApresFini");
	//		a = TransitionType.AfterFinished;
	//		break;

	//	default: throw new System.Exception("Unknown TransitionType");
//	}


//}