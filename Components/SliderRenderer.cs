﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ODP;
using ODP.Utils;
using ODP.API;
using System.Collections;

namespace Slidermax {
	public class SliderRenderer : MonoBehaviour {

		public const float CENTIMETERS_SLIDE_WIDTH = 28f;
		public const float CENTIMETERS_SLIDE_HEIGHT = 15.75f;
		public static readonly Size ODP_SLIDE_HEIGHT = Size.ParseString(CENTIMETERS_SLIDE_HEIGHT + "cm");
		public static readonly Size ODP_SLIDE_WIDTH = Size.ParseString(CENTIMETERS_SLIDE_WIDTH + "cm");
		public const float UNITY_PLANE_DIM_X = 10;
		public const float UNITY_PLANE_DIM_Z = 10;

		// ODP informations
		[Header("Paramétrage du document")]
		[Tooltip("Choisir un fichier ODP présent dans Assets/.")][SerializeField] string filePath = "Docs/test.odp";
		private ODPDocument odp;
		public static int currentDiapo = -1; // commence à 1.
		private ISlide slide;
		// Objects informations
		private float width, height;
		private MeshRenderer meshRenderer;
		[Header("Paramétrage géométrique")]
		[Tooltip("Choisir l'origine du plan (coin en haut à gauche).")] [SerializeField] public Transform origin;
		[Tooltip("Choisir une instance d'un TransitionRenderer.")] [SerializeField] public TransitionsRenderer transitionsRenderer;
		private IList<SlideFrame> frames = new List<SlideFrame>();

        void Awake() {
			odp = ODPParser.ParseODPFile("Assets/" + filePath);
			if (odp.GetSlidesCount() == 0)
				throw new Exception("ODP file without any diapositive.");

			Debug.Log($"Nombre de diapos : {odp.GetSlidesCount()}.");

			currentDiapo = 1;
		}

		void Start() {
			meshRenderer = GetComponent<MeshRenderer>();
			meshRenderer.material.color = Color.white;
			width = transform.localScale.x;
			height = transform.localScale.z;
			ChangeSlide();
		}

		private void ChangeSlide() {
			ChangeSlide(currentDiapo);
		}
		
		private void ChangeSlide(int slideNumber) {
			// On récupère le nouveau style
			slide = odp.GetSlideNumber(slideNumber);
			if (slide == null) { return; } // pas besoin d'erreur, on ignore juste la demande.

			// On nettoie les anciennes diapos
			foreach (SlideFrame frame in frames) {
				frame.Destroy();
			}
			frames.Clear();
			currentDiapo = slideNumber;

			// Reset du décalage sur Y
			cumulY = 0.01f;

			// On charge & affiche les nouvelles diapos.
			if(slide.Mask != null) {
				foreach (IFrame maskFrame in slide.Mask.GetFrames()) {
					AddFrame(maskFrame, "mask");
				}
			}

			cumulY += 1f; // pour les que les petits traucs aient de la marge pour render

			foreach (IFrame apiframe in slide.GetElements()) {
				if (apiframe.GetContent() == null) {
					Debug.Log("Contains empty frame !");
					continue;
				}
				AddFrame(apiframe);
			}
		}

		private const float DELTA = 0.1f;
		private float cumulY = 0.01f;
		private void AddFrame(IFrame apiFrame, string frameName = "frame") {
			SlideFrame frame = new SlideFrame(origin, apiFrame, $"{frameName}_{frames.Count}", new Vector2(width, height), cumulY);
			frames.Add(frame);
			cumulY += DELTA;
		}

		public bool SlideExists(int slideNumber) {
			return odp.GetSlideNumber(slideNumber) != null;
		}

		private void Update() {
			if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetMouseButtonDown(1)) {
				if (SlideExists(currentDiapo + 1)) {
					if(transitionsRenderer != null)
						transitionsRenderer.AnimateRight();
					StartCoroutine(LoadR(transitionsRenderer != null ? .5f : 0));
				}
			}

            if (Input.GetKeyDown(KeyCode.LeftArrow)) {
				if (SlideExists(currentDiapo - 1)) {
					if (transitionsRenderer != null)
						transitionsRenderer.AnimateLeft();
					StartCoroutine( LoadL(transitionsRenderer != null ? .5f : 0) );
				}
			}
        }
		IEnumerator LoadR(float wait) {
            yield return new WaitForSeconds(wait);
			ChangeSlide(currentDiapo + 1);
		}

		IEnumerator LoadL(float wait) {
			yield return new WaitForSeconds(wait);
			ChangeSlide(currentDiapo - 1);
		}
	}
}
