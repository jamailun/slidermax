﻿using ODP.Utils;

namespace Slidermax {
	public class SizeAdaptater {

		private SizeAdaptater() { }

		public const float HOW_MANY_PIXELS_IN_A_CENTIMETER = 10f;

		private static float ConvertSize(Size size) {
			switch (size.Unit) {
				case StyleUnit.Centimeter:
					return size.Value;
				case StyleUnit.Pixel:
					return size.Value * HOW_MANY_PIXELS_IN_A_CENTIMETER;
				case StyleUnit.Percentage:
					throw new System.Exception("Size in percentage unit cannot be converted to absolute mesure.");
			}
			throw new System.Exception("Unexpected size unit : "+size+".");
		}

		// Convert a odp unit dimension to a equivalent scaled from 0 to 1 according to size of odp page.
		// returns result between 0 and 1.
		public static float ConvertODPUnits(Size odpObjectSize, Size sizeOdpCst) {
			if (odpObjectSize.Unit == StyleUnit.Percentage)
				return (sizeOdpCst.Value * sizeOdpCst.Value) / (100f * 2);
			if (odpObjectSize.Unit != sizeOdpCst.Unit)
				throw new System.Exception("ConvertUnit -> not the same units !");
			return odpObjectSize.Value / sizeOdpCst.Value;
		}

	}
}
