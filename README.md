# SLIDERMAX

## Résumé

Étude Pratique de 3INFO de l'INSA de Rennes, 2020-2021.

Il s'agit d'un outil permettant de lire des slides tout en pouvant transitionner avec un environnement 3D Unity.


## Auteurs

- Timothé Rosaz,
- Sofiane Mazieres,
- Thomas Dumoulin
- Emilie Levaique.

## Notes

- Ce projet repose sur l'API [ODPAPI](https://gitlab.com/jamailun/odpapi) par Timothé ROSAZ. Libre de droit et d'utilisation. C'est ce qui permet de traiter les fichiers ODP.

- **NOTE : ** ce projet utilisant des résultats de recherches non déposés dans ce git, celui-ci n'est pas vraiment utilisable...
